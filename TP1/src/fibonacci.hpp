#ifndef FIBONACCI_HPP_
#define FIBONACCI_HPP_
int fibonacciIteratif(int n);
int fibonacciRecursif(int n);

#endif
