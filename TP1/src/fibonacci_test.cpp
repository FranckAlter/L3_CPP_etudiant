#include <cassert>
#include "fibonacci.hpp"
#include <CppUTest/CommandLineTestRunner.h>

TEST_GROUP(GroupFibo) { };

TEST(GroupFibo, test_fibo_1) {  // premier test
    int result = fibonacciRecursif(2);
    CHECK_EQUAL(1, result);
}

TEST(GroupFibo, test_fibo_2) {  // deuxième test
    int result = fibonacciRecursif(3);
    CHECK_EQUAL(2, result);
}

TEST(GroupFibo, test_fibo_3) {  
    int result = fibonacciRecursif(4);
    CHECK_EQUAL(3, result);
}
