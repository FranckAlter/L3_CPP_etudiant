//
// Created by faubry on 06/03/2020.
//
#include "Liste.hpp"
#include "Noeud.hpp"

void Liste::ajouterDevant (int valeur)
{
    Noeud* n = new Noeud (valeur);
    n->_suivant = _tete;
    _tete = n;
}
int Liste::getTaille () const
{
    int compt = 0;
    Noeud* n = _tete;
    while (n != nullptr)
    {
       n = n-> _suivant;
       compt ++;
    }
    return compt;
}
int Liste::getElement (int indice ) const
{
    Noeud *n = _tete;
    while (indice > 0)
    {
        n = n-> _suivant;
        indice --;
    }
    return n->valeur_;
}

Liste::Liste()
{_tete = nullptr;}






