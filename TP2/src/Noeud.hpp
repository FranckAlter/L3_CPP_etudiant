#ifndef NOEUD_HPP
#define NOEUD_HPP


struct Noeud
{
    int valeur_;
    Noeud * _suivant;

    Noeud (int val)
    {
        this->valeur_ = val;
        this->_suivant = nullptr;
    }
};


#endif