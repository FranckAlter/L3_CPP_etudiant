//
// Created by franck on 23/03/2020.
//

#include "Client.h"
#include <iostream>

Client::Client (int id, std::string nom):
        _id (id), _nom (nom) {}

void Client::afficherClient() const {
    std::cout<< "Client (" << _id << ", " << _nom << ")" << std::endl;
}

 int Client::getId() const {
    return _id;
}

 std::string  Client::getNom() const{
    return _nom;
}