//
// Created by franck on 23/03/2020.
//

#ifndef TP3_CLIENT_H
#define TP3_CLIENT_H
#include <iostream>

class Client {
private:
    int _id;
    std::string _nom;

public:
    Client(int id, std::string nom);
    void afficherClient() const;
    int getId() const;
    std::string getNom() const;
};



#endif //TP3_CLIENT_H
