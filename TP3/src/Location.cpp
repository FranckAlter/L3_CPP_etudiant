//
// Created by franck on 23/03/2020.
//

#include "Location.h"
#include <iostream>

Location::Location (int idC, int idP):
    _idClient (idC), _idProduit (idP) {

}

void Location::afficherLocation() const {
    std::cout<< "Location (" << _idClient << ", " << _idProduit << ")" << std::endl;
}

int Location::getClient() const{
    return _idClient;
};

int Location::getProduit() const{
    return _idProduit;
};