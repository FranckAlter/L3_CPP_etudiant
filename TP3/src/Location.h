//
// Created by franck on 23/03/2020.
//

#ifndef TP3_LOCATION_H
#define TP3_LOCATION_H


class Location {
private:
    int _idClient;
    int _idProduit;

public:
    Location(int _idClient, int _idProduit);
    void afficherLocation() const;
    int getClient() const;
    int getProduit() const;
};


#endif //TP3_LOCATION_H
