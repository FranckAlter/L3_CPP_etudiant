//
// Created by franck on 23/03/2020.
//

#include "Magasin.h"
#include <iostream>

Magasin::Magasin ():
        _idCourantClient (0), _idCourantProduit (0) {}

int Magasin::nbClients()
{
    return _clients.size();
};

void Magasin::ajouterClient (const std::string nom)
{
    _clients.push_back(Client(_idCourantClient, nom));
    _idCourantClient ++;
}

void Magasin::afficherClients() const{
    for (const Client &_client : _clients)
    {
        _client.afficherClient();
    }
}



void Magasin::supprimerClient(int idClient) {
    bool verif = false;
    for (const Client &_client : _clients) {
        if (_client.getId() == idClient) {
            verif = true;}
    }
    if (!verif)
    {
        throw std::string("ERROR NOt found client");
    }

    int compt = 0;
    for (const Client &_client : _clients) {
        if (_client.getId() == idClient) {
            _clients.erase(_clients.begin() + compt);
        }
        compt++;
    }

}

void Magasin::supprimerProduit(int idProduit) {
    std::vector<Produit> temp;
    bool verif = false;
    for (const Produit &_produit : _produits) {
        if (_produit.getId() == idProduit) {
            verif = true;}
    }
    if (!verif)
    {
        throw std::string("ERROR NOt found produit");
    }
    int compt = 0;
    for (const Produit &_produit : _produits) {
        if (_produit.getId() == idProduit) {
            _produits.erase(_produits.begin() + compt);
            break;
        }
        compt++;
    }
}

void Magasin::ajouterProduit (const std::string nom)
{
    _produits.push_back(Produit(_idCourantProduit, nom));
    _idCourantProduit ++;
}

void Magasin::afficherProduits() const{
    for (const Produit &_produit : _produits)
    {
        _produit.afficherProduit();
    }
}

int Magasin::nbProduits()
{
    return _produits.size();
};

int Magasin::nbLocations() {
    return _locations.size();
}

void Magasin::ajouterLocations(int idClient, int idProduit) {
    if (_locations.empty()) {
        _locations.push_back(Location(idClient, idProduit));
        return;
    }

    for (const Location &_location: _locations) {
        if (_location.getClient() == idClient && _location.getProduit() == idProduit) {
            throw std::string("ERROR already exists");
        }
    }

    _locations.push_back(Location(idClient, idProduit));
}

void Magasin::afficherLocations() const{
    for (const Location &_location : _locations)
    {
        _location.afficherLocation();
    }
}

void Magasin::supprimerLocation (int client, int produit)
{
    std::vector<Location> temp;
    bool verif = false;
    for (const Location &_location : _locations) {
        if (_location.getClient() == client && _location.getProduit() == produit) {
            verif = true;}
    }
    if (!verif)
    {
        throw std::string("ERROR NOt found location");
    }
    int compt = 0;
    for (const Location &_location : _locations) {
        if (_location.getClient() == client && _location.getProduit() == produit) {
            _locations.erase(_locations.begin() + compt);}
        compt++;
    }
}

bool Magasin::trouverClientDansLocation(int client) {
    for (const Location &_location : _locations){
        if( _location.getClient() == client)
        {
            return true;
        }
    }
    return false;
}

bool Magasin::trouverProduitDansLocation(int produit) {
    for (const Location &_location : _locations){
        if( _location.getProduit() == produit)
        {
            return true;
        }
    }
    return false;
}

std::vector<int> Magasin::calculerClientsLibres() {
    std::vector<int> libres;
    for (const Client &_client : _clients) {
        bool test = false;
        for (const Location &_location : _locations){
            if (_location.getClient() == _client.getId())
            {
                test = true;
            }
        }
        if (!test)
        {
            libres.push_back(_client.getId());
        }
    }
    return libres;
}

std::vector<int> Magasin::calculerProduitsLibres() {
    std::vector<int> libres;
    for (const Produit &_produit : _produits) {
        bool test = false;
        for (const Location &_location : _locations){
            if (_location.getProduit() == _produit.getId())
            {
                test = true;
            }
        }
        if (!test)
        {
            libres.push_back(_produit.getId());
        }
    }
    return libres;
}