//
// Created by franck on 23/03/2020.
//

#ifndef TP3_MAGASIN_H
#define TP3_MAGASIN_H
#include <iostream>
#include <vector>
#include "Client.h"
#include "Produit.h"
#include "Location.h"

class Magasin {
private:
    std::vector<Client> _clients;
    std::vector<Produit> _produits;
    std::vector<Location> _locations;
    int _idCourantClient;
    int _idCourantProduit;

public:
    Magasin();
    int nbClients();
    void ajouterClient (const std::string nom );
    void afficherClients () const;
    void supprimerClient (int idclient);
    int nbProduits();
    void ajouterProduit (const std::string nom );
    void afficherProduits () const;
    void supprimerProduit (int idprod);
    int nbLocations();
    void ajouterLocations(int idClient, int idProduit);
    void afficherLocations() const;
    void supprimerLocation(int idClient, int idProduit);
    bool trouverClientDansLocation(int idClient);
    std::vector<int> calculerClientsLibres();
    bool trouverProduitDansLocation(int idProduit);
    std::vector<int> calculerProduitsLibres();



};


#endif //TP3_MAGASIN_H
