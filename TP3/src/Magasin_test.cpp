//
// Created by franck on 23/03/2020.
//
#include <CppUTest/CommandLineTestRunner.h>
#include "Magasin.h"


TEST_GROUP(GroupMagasin) {
};


TEST(GroupMagasin, ajoutClient) {
Magasin magasin = Magasin();
magasin.ajouterClient("Franck");
CHECK_EQUAL(magasin.nbClients(), 1);
}

TEST(GroupMagasin, suppClient) {
Magasin magasin = Magasin();
magasin.ajouterClient("Franck");
magasin.supprimerClient(0);
CHECK_EQUAL(magasin.nbClients(), 0);
}



TEST(GroupMagasin, ajoutProduit) {
Magasin magasin = Magasin();
magasin.ajouterProduit("PQ");
CHECK_EQUAL(magasin.nbProduits(), 1);
}

TEST(GroupMagasin, suppProduit) {
Magasin magasin = Magasin();
magasin.ajouterProduit("PQ");
magasin.supprimerProduit(0);
CHECK_EQUAL(0, magasin.nbProduits());
}

TEST(GroupMagasin, booltest) {
Magasin magasin = Magasin();
magasin.ajouterProduit("PQ");
magasin.ajouterClient("Franck");
magasin.ajouterLocations(0, 0);
CHECK_TRUE(magasin.trouverClientDansLocation(0))
CHECK_TRUE(magasin.trouverProduitDansLocation(0))
}

TEST(GroupMagasin, produitDispo) {
Magasin magasin;
magasin.ajouterClient("Franck");
magasin.ajouterProduit("PQ");
magasin.ajouterProduit("Pâtes");
magasin.ajouterProduit("Masque");
magasin.ajouterProduit("Barbecue");
magasin.ajouterProduit("Chaussures");
magasin.afficherProduits();
magasin.afficherClients();
magasin.ajouterLocations(0, 0);
magasin.ajouterLocations(0, 1);
magasin.ajouterLocations(0, 2);
CHECK_EQUAL(3,magasin.calculerProduitsLibres()[0]);
CHECK_EQUAL(4,magasin.calculerProduitsLibres()[1]);
}
