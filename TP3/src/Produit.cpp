//
// Created by franck on 23/03/2020.
//

#include "Produit.h"
#include <iostream>

Produit::Produit (int id, std::string description):
        _id (id), _description (description) {}

void Produit::afficherProduit() const {
    std::cout<< "Produit (" << _id << ", " << _description << ")" << std::endl;
}

int Produit::getId() const {
    return _id;
}

std::string  Produit::getDescription() const{
    return _description;
}