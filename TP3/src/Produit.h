//
// Created by franck on 23/03/2020.
//

#ifndef TP3_PRODUIT_H
#define TP3_PRODUIT_H
#include <iostream>


class Produit {
private:
    int _id;
    std::string _description;

public:
    Produit(int id, std::string description);
    void afficherProduit() const;
    int getId() const;
    std::string getDescription() const;
};


#endif //TP3_PRODUIT_H
