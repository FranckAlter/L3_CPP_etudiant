//
// Created by franck on 23/03/2020.
//

#include "Location.h"
#include "Client.h"
#include "Produit.h"
#include "Magasin.h"

int main() {
    Location test (0, 2);
    test.afficherLocation();
    Client toto (0, "Franck");
    toto.afficherClient();
    Produit bonbon (0, "bonbon");
    bonbon.afficherProduit();
    Magasin darty ;
    std::cout<<darty.nbClients()<<std::endl;
    darty.ajouterClient("Peter Parker");
    std::cout<<darty.nbClients()<<std::endl;
    darty.ajouterClient("Michel Drucker");
    std::cout<<darty.nbClients()<<std::endl;
    darty.supprimerClient(1);
    std::cout<<darty.nbClients()<<std::endl;
}