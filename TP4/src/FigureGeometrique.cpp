//
// Created by franck on 23/03/2020.
//

#include "FigureGeometrique.hpp"

FigureGeometrique::FigureGeometrique(Couleur couleur):
_couleur (couleur){}

Couleur FigureGeometrique::getCouleur() {
    return _couleur;
}