//
// Created by franck on 23/03/2020.
//

#ifndef TP4_FIGUREGEOMETRIQUE_HPP
#define TP4_FIGUREGEOMETRIQUE_HPP


#include "Couleur.hpp"

class FigureGeometrique {
protected:
    Couleur _couleur;

public:
    FigureGeometrique(Couleur couleur);
    Couleur getCouleur();

};


#endif //TP4_FIGUREGEOMETRIQUE_HPP
