#include "Controleur.hpp"
#include "VueConsole.hpp"

#include <cassert>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

Controleur::Controleur(int argc, char **argv) {
    _vues.push_back(std::make_unique<VueConsole>(argc, argv, *this));
    _vues.push_back(std::make_unique<VueGraphique>(argc, argv, *this));
    _inventaire._bouteilles.push_back(Bouteille{"Pochtron", "2011-09-17", 8.25});
    std::cout<<"coucou0"<<std::endl;
    this->chargerInventaire("../mesBouteilles.txt");
    for (auto &v : _vues)
        v->actualiser();
}

void Controleur::run() {
    for (auto &v : _vues)
        v->run();
}

std::string Controleur::getTexte() {
    std::ostringstream ostringstream;
    ostringstream << _inventaire;
    return ostringstream.str();
}
    void Controleur::chargerInventaire(const std::string fichier) {
        std::cout<<"coucou"<<std::endl;
        std::ifstream file(fichier);
        if (file.is_open()) {
            std::cout<<"coucou2"<<std::endl;
            std::string ligne;
            Bouteille bouteille;
            while (getline(file, ligne)) {
                file >> bouteille;
                _inventaire._bouteilles.push_back(bouteille);
            }
        }
    }




