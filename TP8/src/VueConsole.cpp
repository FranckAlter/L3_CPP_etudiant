//
// Created by franck on 30/04/2020.
//

#include "VueConsole.hpp"
VueConsole::VueConsole(int argc, char ** argv, Controleur &controleur) : Vue(controleur) {}

void VueConsole::actualiser() {
    std::cout << _controleur.getTexte() << std::endl;
}

void VueConsole::run() {}
