//
// Created by franck on 30/04/2020.
//

#ifndef TP8_VUECONSOLE_HPP
#define TP8_VUECONSOLE_HPP


#include "Controleur.hpp"

class VueConsole : public Vue {

public:

    VueConsole(int argc, char ** argv, Controleur &controleur);

    void actualiser();

    void run();

};




#endif //TP8_VUECONSOLE_HPP
